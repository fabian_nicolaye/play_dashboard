<?php

namespace PLAY\PlayDashboard\Controller;

class CreditsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * submitSongRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\SubmitSongRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $submitSongRepository;

    /**
     * creditsRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\CreditsRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $creditsRepository;

    /**
     * Show action
     */
    public function showAction()
    {
        $this->view->assign('userData', $GLOBALS['TSFE']->fe_user->user);
    }

    public function createAction()
    {

        if ($this->request->getArguments()['changePlan']['plan'] == 'free'){

            $coins = $this->submitSongRepository->getUserCredits($GLOBALS['TSFE']->fe_user->user['uid']);

            $coins = $coins['0']['credits'] + 2;

            $this->creditsRepository->changePlan($GLOBALS['TSFE']->fe_user->user['uid'], 'free', $coins);
        }
    }

    public function freeAction()
    {

    }

    public function starterAction()
    {

    }

    public function professionalAction()
    {

    }

    public function proArtistAction()
    {

    }

}
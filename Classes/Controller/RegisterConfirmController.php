<?php

namespace PLAY\PlayDashboard\Controller;


class RegisterConfirmController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * registerRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\RegisterRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $registerRepository;

    /**
     * Show action
     */
    public function showAction()
    {

        $user = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user');
        $token = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('token');

        if ($token < strtotime('+24 hours') && \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('confirm')) {
            $this->view->assign('confirm', true);
            $this->registerRepository->confirmAccount($user);

        } else {
            $this->view->assign('confirm', false);
        }
    }
}
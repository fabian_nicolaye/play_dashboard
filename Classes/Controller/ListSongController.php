<?php

namespace PLAY\PlayDashboard\Controller;

class ListSongController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * submitSongRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\SubmitSongRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $submitSongRepository;

    /**
     * submittedSongsRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\SubmittedSongsRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $submittedSongsRepository;

    /**
     * Show action
     */
    public function showAction()
    {
        if ($this->request->getArguments()['deleted'] == 1) {
            $this->submittedSongsRepository->deleteSong($this->request->getArguments()['uid']);
        }

        $songDatas = $this->submitSongRepository->listSongs($GLOBALS['TSFE']->fe_user->user['uid']);

        $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();

        $songCoverAndFile = [];
        foreach ($songDatas as $songData) {
            $songFile = $songData['song_file'];
            $songCover = $songData['song_cover'];
            $songCover = $resourceFactory->getFileObject($songCover);
            $songFile = $resourceFactory->getFileObject($songFile);
            $songCoverAndFile = [
                'song_cover' => $songCover,
                'song_file' => $songFile
            ];
            $basket[] = array_replace($songData, $songCoverAndFile);
        }

        $this->view->assignMultiple([
            'songs' => $basket,
            'credits' => $this->submitSongRepository->getUserCredits($GLOBALS['TSFE']->fe_user->user['uid']),
            'verified' => $GLOBALS['TSFE']->fe_user->user['verified'],
            'cover_and_file' => $songCoverAndFile,
            'userData' => $GLOBALS['TSFE']->fe_user->user
        ]);
    }
}
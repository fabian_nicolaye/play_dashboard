<?php

namespace PLAY\PlayDashboard\Controller;

use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Mpdf\Mpdf;


class VerifyController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * registerRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\RegisterRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $registerRepository;

    /**
     * Show action
     */
    public function showAction()
    {
        $this->view->assign('verified', $GLOBALS['TSFE']->fe_user->user['verified']);
        $this->view->assign('userData', $GLOBALS['TSFE']->fe_user->user);
    }

    /**
     * Create action
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function createAction()
    {
        $userData = $this->registerRepository->getUserData($GLOBALS['TSFE']->fe_user->user['uid']);
        if ($userData['0']['register_token'] == $this->request->getArguments()['code']) {
            $this->view->assign('artist', $GLOBALS['TSFE']->fe_user->user);
            $this->sendMail($GLOBALS['TSFE']->fe_user->user['email'], $GLOBALS['TSFE']->fe_user->user['artist_name']);

        } else {
            $uriBuilder = $this->uriBuilder;
            $uri = $uriBuilder
                    ->setTargetPageUid(23)
                    ->build();
            $this->redirectToUri($uri);
        }

    }

    /**
     * @param $email
     * @param $fullName
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendMail($email, $fullName)
    {
        $email = GeneralUtility::makeInstance(FluidEmail::class)
                ->from(new \Symfony\Component\Mime\Address('no-reply@extone-records.com', 'PLAY by Extone-Records'))
                ->to($email)
                ->setTemplate('AccountVerified')
                ->assign('name', $fullName)
                ->format(FluidEmail::FORMAT_HTML)
                ->subject('Account Verified');
        GeneralUtility::makeInstance(Mailer::class)->send($email);
    }

    public function completeAction()
    {
        $this->registerRepository->verifyAccount($GLOBALS['TSFE']->fe_user->user['uid']);
    }
}
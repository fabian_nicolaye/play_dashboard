<?php

namespace PLAY\PlayDashboard\Controller;

class EditSongController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * submitSongRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\SubmitSongRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $submitSongRepository;

    /**
     * editProfileRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\EditProfileRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $editProfileRepository;

    /**
     * editSongRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\EditSongRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $editSongRepository;

    /**
     * Show action
     * @throws \TYPO3\CMS\Core\Resource\Exception\FileDoesNotExistException
     */
    public function showAction()
    {
        $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();

        $song = $this->editSongRepository->showSong(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('uid'));

        foreach ($song as $songData) {
            $songFile = $songData['song_file'];
            $songCover = $songData['song_cover'];
            $songCover = $resourceFactory->getFileObject($songCover);
            $songFile = $resourceFactory->getFileObject($songFile);
            $songCoverAndFile = [
                    'song_cover' => $songCover,
                    'song_file' => $songFile
            ];
            $song[] = array_replace($songData, $songCoverAndFile);
        }

        foreach ($song as $songData) {
            $this->view->assign('song', $songData);
        }
        $this->view->assign('userData', $GLOBALS['TSFE']->fe_user->user);

        $songIdentifier = $songData['song_file'];
        $this->view->assign('songIdentifier', $songIdentifier->getIdentifier());
    }
}
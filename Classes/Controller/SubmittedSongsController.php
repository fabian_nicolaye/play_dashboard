<?php

namespace PLAY\PlayDashboard\Controller;

class SubmittedSongsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * submitSongRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\SubmitSongRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $submitSongRepository;

    /**
     * submittedSongsRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\SubmittedSongsRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $submittedSongsRepository;

    /**
     * editSongRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\EditSongRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $editSongRepository;

    /**
     * Show action
     */
    public function showAction()
    {

        if ($this->request->getArguments()['deleted'] == 1) {
            $this->submittedSongsRepository->deleteSong($this->request->getArguments()['uid']);
        }
        $this->view->assign('songs', $this->submittedSongsRepository->showSongs());
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function editAction()
    {
        $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();

        $song = $this->editSongRepository->showSong($this->request->getArgument('uid'));
        $userUid = $song['0']['fe_user'];
        $userData = $this->editSongRepository->getUserDataById($userUid);

        $songFile = $song['0']['song_file'];
        $songCover = $song['0']['song_cover'];
        $songCover = $resourceFactory->getFileObject($songCover);
        $songFile = $resourceFactory->getFileObject($songFile);
        $songCoverAndFile = [
            'song_cover' => $songCover,
            'song_file' => $songFile
        ];
        $song['0'] = array_replace($song['0'], $songCoverAndFile);

        $this->view->assign('song', $song['0']);
        $this->view->assign('userData', $userData['0']);

        if ($this->request->getArguments()['approved'] == 1) {
            $this->approveTrack($song['0']['uid']);
        }
        if ($this->request->getArguments()['approved'] == 4) {
            $this->rejectTrack($song['0']['uid']);
        }
    }

    /**
     * @param $uid
     */
    public function approveTrack($uid)
    {
        $this->submittedSongsRepository->approveSong($uid);

    }

    /**
     * @param $uid
     */
    public function rejectTrack($uid)
    {
        $this->submittedSongsRepository->rejectSong($uid);
    }
}
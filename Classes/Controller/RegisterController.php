<?php

namespace PLAY\PlayDashboard\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Crypto\PasswordHashing\PasswordHashFactory ;
use Symfony\Component\Mime\Address;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;

class RegisterController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * registerRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\RegisterRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $registerRepository;

    /**
     * submitSongRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\SubmitSongRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $submitSongRepository;

    /**
     * Show action
     */
    public function createAction()
    {
        $userData = $this->request->getArguments()['register'];

        if (isset($userData['password'])) {
            if (isset($userData['password']) == $userData['passwordRepeat']) {
                $registerToken = rand(1000, 9999);
                $this->createAccount(
                        $userData['fullName'],
                        $userData['artistName'],
                        $userData['email'],
                        $userData['password'],
                        $registerToken
                );
                $this->view->assign('success', 'true');

            } else {
                $this->view->assign('failed', 1);
            }
        }
    }

    /**
     * @param $fullName
     * @param $artistName
     * @param $email
     * @param $phone
     * @param $street
     * @param $zip
     * @param $city
     * @param $password
     * @param $registerToken
     * @throws \TYPO3\CMS\Core\Crypto\PasswordHashing\InvalidPasswordHashException
     */
    public function createAccount($fullName, $artistName, $email, $password, $registerToken)
    {
        $password = $this->hashPassword($password);
        $this->registerRepository->createAccount($fullName, $artistName, $email, $password, $registerToken);
        $this->sendMail($email, $fullName, $registerToken);


    }

    /**
     * @param $password
     * @return mixed
     * @throws \TYPO3\CMS\Core\Crypto\PasswordHashing\InvalidPasswordHashException
     */
    public function hashPassword($password)
    {
        $hashInstance = GeneralUtility::makeInstance(PasswordHashFactory::class)->getDefaultHashInstance('FE');
        return $hashInstance->getHashedPassword($password);

    }

    /**
     * @param $email
     * @param $fullName
     * @param $registerToken
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendMail($email, $fullName, $registerToken) {
        $email = GeneralUtility::makeInstance(FluidEmail::class)
                ->from(new \Symfony\Component\Mime\Address('no-reply@extone-records.com', 'PLAY by Extone-Records'))
                ->to($email)
                ->setTemplate('CompleteRegestration')
                ->assign('name', $fullName)
                ->format(FluidEmail::FORMAT_HTML)
                ->subject('Complete Registration')
                ->assign('token', $registerToken);
        GeneralUtility::makeInstance(Mailer::class)->send($email);
    }

    public function confirmAction()
    {

    }
}
<?php

namespace PLAY\PlayDashboard\Controller;

use \PLAY\PlayDashboard\Domain\Repository;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use TYPO3\CMS\Core\Resource\Exception\ExistingTargetFolderException;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderWritePermissionsException;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\StorageRepository;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class SubmitSongController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * submitSongRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\SubmitSongRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $submitSongRepository;

    /**
     * submittedSongsRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\SubmittedSongsRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $submittedSongsRepository;

    /**
     * Show action
     */
    public function showAction()
    {
        if ($this->request->getArguments()['deleted'] == 1) {
            $this->submittedSongsRepository->deleteSong($this->request->getArguments()['uid']);
        }

        $this->view->assign('userData', $GLOBALS['TSFE']->fe_user->user);
        $credits = $this->submitSongRepository->getUserCredits($GLOBALS['TSFE']->fe_user->user['uid'])['0']['credits'];
        $this->view->assign('credits', $credits);
    }

    /**
     * Create Action
     * @throws \TYPO3\CMS\Core\Resource\Exception\ExistingTargetFileNameException
     * @throws InsufficientFolderAccessPermissionsException
     */
    public function createAction()
    {
        $credits = $this->submitSongRepository->getUserCredits($GLOBALS['TSFE']->fe_user->user['uid'])['0']['credits'];

        $titleNormal = $this->request->getArguments()['title'];
        $title = str_replace(" ", "_", $titleNormal);

        $songData = $this->upload($this->request->getArguments()['song'], $title);
        $coverData = $this->upload($this->request->getArguments()['cover'], $title);

        $songUid = $songData->getProperties()['uid'];
        $coverUid = $coverData->getProperties()['uid'];

        $this->submitSongRepository->submitSong($GLOBALS['TSFE']->fe_user->user['uid'], $titleNormal, $coverUid, $songUid, $this->request->getArguments()['feature']);

        $newCredits = $credits - 1;
        $this->submitSongRepository->reduceUserCredits($GLOBALS['TSFE']->fe_user->user['uid'], $newCredits);

        $this->view->assign('data', $this->request->getArguments());
        $this->view->assign('coverData', $coverData);
        $this->view->assign('songData', $songData->getIdentifier());
        $this->view->assign('artistName', $GLOBALS['TSFE']->fe_user->user['artist_name']);

        $this->sendMail($GLOBALS['TSFE']->fe_user->user['email'], $GLOBALS['TSFE']->fe_user->user['artist_name']);
    }

    /**
     * @param $file
     * @param $title
     * @return \TYPO3\CMS\Core\Resource\File|\TYPO3\CMS\Core\Resource\FileInterface
     * @throws InsufficientFolderAccessPermissionsException
     * @throws \TYPO3\CMS\Core\Resource\Exception\ExistingTargetFileNameException
     */
    public function upload($file, $title)
    {
        $resourceFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Resource\ResourceFactory::class);
        $storage = $resourceFactory->getDefaultStorage();

        try {
            $storage->createFolder('Covers/');
        } catch (ExistingTargetFolderException | InsufficientFolderWritePermissionsException | InsufficientFolderAccessPermissionsException | \Exception $e) {
        }
        try {
            $storage->createFolder('Tracks/');
        } catch (ExistingTargetFolderException | InsufficientFolderWritePermissionsException | InsufficientFolderAccessPermissionsException | \Exception $e) {
        }

        if ($file['type'] == 'audio/wav') {
            return $storage->addFile(
                    $file['tmp_name'],
                    $storage->getFolder('Tracks'),
                    $title . '_' .  $file['name']
            );
        } else {
             return $storage->addFile(
                    $file['tmp_name'],
                    $storage->getFolder('Covers'),
                    $title . '_' .  $file['name']
            );
        }

    }

    public function sendMail($email, $fullName)
    {
        $email = GeneralUtility::makeInstance(FluidEmail::class)
                ->from(new \Symfony\Component\Mime\Address('no-reply@extone-records.com', 'PLAY by Extone-Records'))
                ->to($email)
                ->setTemplate('SongSubmitted')
                ->assign('name', $fullName)
                ->format(FluidEmail::FORMAT_HTML)
                ->subject('Song Submitted');
        GeneralUtility::makeInstance(Mailer::class)->send($email);

    }
}
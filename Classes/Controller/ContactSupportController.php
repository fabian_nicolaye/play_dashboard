<?php

namespace PLAY\PlayDashboard\Controller;

use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ContactSupportController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * contactSupportRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\ContactSupportRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $contactSupportRepository;

    /**
     * Show Action
     */
    public function showAction()
    {
        $messageDataAdmin = $this->contactSupportRepository->listMessagesFromAdmin($GLOBALS['play_dashboard']['AdminUid'], $GLOBALS['TSFE']->fe_user->user['uid']);
        $messageDataUser = $this->contactSupportRepository->listMessagesFromUser($GLOBALS['TSFE']->fe_user->user['uid'], $GLOBALS['play_dashboard']['AdminUid']);

        $this->view->assignMultiple(
            [
                'messageAdmin' => $messageDataAdmin,
                'messageUser' => $messageDataUser
            ],
        );
    }

    /**
     * Create Action
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */
    public function createAction()
    {
        $messageData = $this->request->getArguments();

        if ($messageData['message']) {
            $this->contactSupportRepository->sendMessage($GLOBALS['TSFE']->fe_user->user['uid'], $messageData['message'], $GLOBALS['play_dashboard']['AdminUid']);
            GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class)
                ->from(new \Symfony\Component\Mime\Address('no-reply@extone-records.com', 'PLAY by Extone-Records'))
                ->to('info@extone-records.com')
                ->text('New Message From: "' .
                    $GLOBALS['TSFE']->fe_user->user['email'] .
                    '" Message: ' . $messageData['message']
                )
                ->subject('New Message')
                ->send();
        }
        $uriBuilder = $this->uriBuilder;
        $uri = $uriBuilder
                ->setTargetPageUid(24)
                ->build();
        $this->redirectToUri($uri, 0, 404);
    }

}
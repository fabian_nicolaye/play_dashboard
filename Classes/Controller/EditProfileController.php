<?php

namespace PLAY\PlayDashboard\Controller;

class EditProfileController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * submitSongRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\SubmitSongRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $submitSongRepository;

    /**
     * editProfileRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\EditProfileRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $editProfileRepository;

    /**
     * Show action
     */
    public function showAction()
    {
        $credits = $this->submitSongRepository->getUserCredits($GLOBALS['TSFE']->fe_user->user['uid'])['0']['credits'];
        $verified = $this->submitSongRepository->getVerifiedStatus($GLOBALS['TSFE']->fe_user->user['uid'])['0']['verified'];
        $this->view->assignMultiple([
                'fe_user_name' => $GLOBALS['TSFE']->fe_user->user['name'],
                'fe_user_title' => $GLOBALS['TSFE']->fe_user->user['title'],
                'fe_user_company' => $GLOBALS['TSFE']->fe_user->user['company'],
                'fe_user_mail' => $GLOBALS['TSFE']->fe_user->user['email'],
                'fe_user_phone' => $GLOBALS['TSFE']->fe_user->user['mobilephone'],
                'fe_user_street' => $GLOBALS['TSFE']->fe_user->user['address'],
                'fe_user_zip' => $GLOBALS['TSFE']->fe_user->user['zip'],
                'fe_user_city' => $GLOBALS['TSFE']->fe_user->user['city'],
                'fe_user_credits' => $credits,
                'songs' => $this->submitSongRepository->listSongs($GLOBALS['TSFE']->fe_user->user['uid']),
                'verified' => $verified
        ]);
    }

    /**
     *
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */
    public function createAction()
    {
        $uid = $GLOBALS['TSFE']->fe_user->user['uid'];
        $fullName = $this->request->getArguments()['editProfile']['fullName'];
        $email = $this->request->getArguments()['editProfile']['email'];
        $phone = $this->request->getArguments()['editProfile']['phone'];
        $street = $this->request->getArguments()['editProfile']['street'];
        $zip = $this->request->getArguments()['editProfile']['zip'];
        $city = $this->request->getArguments()['editProfile']['city'];

        $this->editProfileRepository->editProfile($uid, $fullName, $email, $phone, $street, $zip, $city);

        $uriBuilder = $this->uriBuilder;
        $uri = $uriBuilder
                ->setTargetPageUid(3)
                ->setArguments(array('safed' => true))
                ->build();
        $this->redirectToUri($uri, 0, 404);

    }
}
<?php

namespace PLAY\PlayDashboard\Controller;

class PlayDashboardController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * submitSongRepository
     * @var \PLAY\PlayDashboard\Domain\Repository\SubmitSongRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $submitSongRepository;

    /**
     * EditProfileController
     * @var \PLAY\PlayDashboard\Controller\EditProfileController
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $editProfileController;

    /**
     * Show action
     */
    public function showAction()
    {
        $this->view->assign('userData',$GLOBALS['TSFE']->fe_user->user );
        $this->view->assignMultiple([
                'songs' => $this->submitSongRepository->listSongs($GLOBALS['TSFE']->fe_user->user['uid']),
                'safed' => \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('safed')
        ]);

        if ($GLOBALS['TSFE']->fe_user->user['instagram']) {
            \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump('$var');
        }
    }

    /**
     * Instagram action
     */
    public function instagramAction()
    {

        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($test2);
        exit;
        $uriBuilder = $this->uriBuilder;
        $uri = $uriBuilder
                ->setTargetPageUid(3)
                ->build();
        $this->redirectToUri($uri, 0, 404);
    }
}
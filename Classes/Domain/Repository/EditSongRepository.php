<?php

namespace PLAY\PlayDashboard\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class EditSongRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @param $uid
     * @return array
     */
    public function showSong($uid)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable('tx_playdashboard_domain_model_song')->createQueryBuilder();

        $statement = $queryBuilder
                ->select('uid','crdate', 'song_name', 'status', 'song_file', 'song_cover', 'feature', 'fe_user')
                ->from('tx_playdashboard_domain_model_song')
                ->where(
                        $queryBuilder->expr()->eq('uid', $uid)
                )
                ->andWhere(
                        $queryBuilder->expr()->eq('deleted', 0)
                )
                ->execute();
        return $statement->fetchAll();
    }

    /**
     * @param $uid
     * @return mixed[]
     */
    public function getUserDataById($uid)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable('fe_users')->createQueryBuilder();

        $statement = $queryBuilder
                ->select('*')
                ->from('fe_users')
                ->where(
                        $queryBuilder->expr()->eq('uid', $uid)
                )
                ->andWhere(
                        $queryBuilder->expr()->eq('deleted', 0)
                )
                ->execute();
        return $statement->fetchAll();
    }
}

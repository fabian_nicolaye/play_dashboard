<?php

namespace PLAY\PlayDashboard\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class RegisterRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @param $fullName
     * @param $artistName
     * @param $email
     * @param $phone
     * @param $street
     * @param $zip
     * @param $city
     * @param $password
     */
    public function createAccount($fullName, $artistName, $email, $password, $registerToken)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
        $affectedRows = $queryBuilder
                ->insert('fe_users')
                ->values([
                        'name' => $fullName,
                        'username' => $email,
                        'email' => $email,
                        'password' => $password,
                        'pid' => 4,
                        'usergroup' => 1,
                        'artist_name' => $artistName,
                        'register_token' => $registerToken
                ])
                ->execute();
    }

    /**
     * @param $email
     */
    public function confirmAccount($email) {

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
        $queryBuilder
                ->update('fe_users')
                ->where(
                        $queryBuilder->expr()->eq('email', $queryBuilder->createNamedParameter($email))
                )
                ->set('disable', 0)
                ->execute();
    }

    /**
     * @param $uid
     */
    public function verifyAccount($uid)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
        $queryBuilder
                ->update('fe_users')
                ->where(
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid))
                )
                ->set('verified', 1)
                ->execute();
    }

    /**
     * @param $mail
     * @return array
     */
    public function getUserData($uid)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable('fe_users')->createQueryBuilder();

        return $queryBuilder
                ->select('*')
                ->from('fe_users')
                ->where(
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid))
                )
                ->execute()
                ->fetchAll();
    }
}

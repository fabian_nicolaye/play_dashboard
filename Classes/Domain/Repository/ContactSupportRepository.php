<?php

namespace PLAY\PlayDashboard\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ContactSupportRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    public function listMessagesFromAdmin($from_user, $to_user): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable('tx_playdashboard_domain_model_messages')->createQueryBuilder();

        $statement = $queryBuilder
                ->select('message', 'crdate')
                ->from('tx_playdashboard_domain_model_messages')
                ->orderBy('uid', 'DESC')
                ->where(
                        $queryBuilder->expr()->eq('to_user_id', $to_user)
                )
                ->andWhere(
                        $queryBuilder->expr()->eq('from_user_id', $from_user)
                )
                ->execute();
        return $statement->fetchAll();
    }

    public function listMessagesFromUser($from_user, $to_user): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable('tx_playdashboard_domain_model_messages')->createQueryBuilder();

        $statement = $queryBuilder
                ->select('message', 'crdate')
                ->from('tx_playdashboard_domain_model_messages')
                ->orderBy('uid', 'DESC')
                ->where(
                        $queryBuilder->expr()->eq('to_user_id', $to_user)
                )
                ->andWhere(
                        $queryBuilder->expr()->eq('from_user_id', $from_user)
                )
                ->execute();
        return $statement->fetchAll();
    }

    /**
     * @param $fromUser
     * @param $message
     * @param $toUser
     */
    public function sendMessage($fromUser, $message, $toUser)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_playdashboard_domain_model_messages');
        $queryBuilder
                ->insert('tx_playdashboard_domain_model_messages')
                ->values([
                        'from_user_id' => $fromUser,
                        'message' => $message,
                        'to_user_id' => $toUser,
                        'crdate' => time(),
                        'tstamp' => time()
                ])
                ->execute();
    }
}

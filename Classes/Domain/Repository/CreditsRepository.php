<?php

namespace PLAY\PlayDashboard\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class CreditsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @param $uid
     * @param $plan
     * @param $coins
     */
    public function changePlan($uid, $plan, $credits)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
        $queryBuilder
                ->update('fe_users')
                ->where(
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid))
                )
                ->set('plan', $plan)
                ->set('credits', $credits)
                ->execute();
    }
}

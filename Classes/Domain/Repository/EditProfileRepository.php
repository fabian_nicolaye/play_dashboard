<?php

namespace PLAY\PlayDashboard\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class EditProfileRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @param $fe_user
     * @param $name
     * @param $email
     * @param $mobilephone
     * @param $address
     * @param $zip
     * @param $city
     */
    public function editProfile($fe_user, $name, $email, $mobilephone, $address, $zip, $city)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
        $queryBuilder
                ->update('fe_users')
                ->where(
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($fe_user))
                )
                ->set('name', $name)
                ->set('email', $email)
                ->set('mobilephone', $mobilephone)
                ->set('address', $address)
                ->set('zip', $zip)
                ->set('city', $city)
                ->execute();
    }
}

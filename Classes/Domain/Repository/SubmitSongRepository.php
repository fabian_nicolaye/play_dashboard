<?php

namespace PLAY\PlayDashboard\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class SubmitSongRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @param $feUser
     * @param $songName
     * @param $songUrl
     */
    public function submitSong($feUser, $songName, $songCover, $songFile, $feature)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_playdashboard_domain_model_song');
        $queryBuilder
                ->insert('tx_playdashboard_domain_model_song')
                ->values([
                        'fe_user' => $feUser,
                        'song_cover' => $songCover,
                        'song_file' => $songFile,
                        'song_name' => $songName,
                        'crdate' => time(),
                        'tstamp' => time(),
                        'feature' => $feature
                ])
                ->execute();
    }

    /**
     * @param $fe_user
     * @return array
     */
    public function listSongs($fe_user): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable('tx_playdashboard_domain_model_song')->createQueryBuilder();

        $statement = $queryBuilder
                ->select('uid', 'crdate', 'song_name', 'status', 'song_file', 'song_cover', 'feature')
                ->from('tx_playdashboard_domain_model_song')
                ->orderBy('uid', 'DESC')
                ->where(
                        $queryBuilder->expr()->eq('fe_user', $fe_user)
                )
                ->andWhere(
                        $queryBuilder->expr()->eq('deleted', 0)
                )
                ->execute();
        return $statement->fetchAll();
    }

    /**
     * @param $fe_user
     * @return array
     */
    public function getUserCredits($fe_user): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable('fe_users')->createQueryBuilder();

        $statement = $queryBuilder
                ->select('credits')
                ->from('fe_users')
                ->where(
                        $queryBuilder->expr()->eq('uid', $fe_user)
                )
                ->execute();
        return $statement->fetchAll();
    }

    /**
     * @param $fe_user
     * @param $credits
     */
    public function reduceUserCredits($fe_user, $credits)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
        $queryBuilder
            ->update('fe_users')
            ->where(
                $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($fe_user))
            )
            ->set('credits', $credits)
            ->execute();
    }

    public function getVerifiedStatus($fe_user)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable('fe_users')->createQueryBuilder();

        $statement = $queryBuilder
                ->select('verified')
                ->from('fe_users')
                ->where(
                        $queryBuilder->expr()->eq('uid', $fe_user)
                )
                ->execute();
        return $statement->fetchAll();
    }
}

<?php

namespace PLAY\PlayDashboard\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class SubmittedSongsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @return array
     */
    public function showSongs()
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable('tx_playdashboard_domain_model_song')->createQueryBuilder();

        $statement = $queryBuilder
                ->select('uid','fe_user','crdate', 'song_name', 'status')
                ->from('tx_playdashboard_domain_model_song')
                ->where(
                        $queryBuilder->expr()->eq('deleted', 0)
                )
                ->execute();
        return $statement->fetchAll();
    }

    /**
     * @param $uid
     */
    public function approveSong($uid)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_playdashboard_domain_model_song');
        $queryBuilder
                ->update('tx_playdashboard_domain_model_song')
                ->where(
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid))
                )
                ->set('status', 1)
                ->execute();
    }

    /**
     * @param $uid
     */
    public function rejectSong($uid)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_playdashboard_domain_model_song');
        $queryBuilder
                ->update('tx_playdashboard_domain_model_song')
                ->where(
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid))
                )
                ->set('status', 4)
                ->execute();
    }

    public function deleteSong($uid)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_playdashboard_domain_model_song');
        $queryBuilder
                ->update('tx_playdashboard_domain_model_song')
                ->where(
                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid))
                )
                ->set('deleted', 1)
                ->execute();
    }
}

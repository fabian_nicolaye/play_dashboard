<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Play Dashboard',
    'description' => '',
    'category' => 'plugin',
    'author' => 'Fabian Nicolaye',
    'author_email' => 'nicolaye@extone-records.com',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.10',
    'constraints' => [
        'depends' => [
            'typo3' => '10',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];

<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'PLAY.PlayDashboard',
    'Dashboard',
    [
        'PlayDashboard' => 'show,instagram'
    ],
    [
        'PlayDashboard' => 'show,instagram'
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'PLAY.PlayDashboard',
    'Listsong',
    [
        'ListSong' => 'show'
    ],
    [
        'ListSong' => 'show'
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'PLAY.PlayDashboard',
        'Submitsong',
        [
                'SubmitSong' => 'show,create'
        ],
        [
                'SubmitSong' => 'show,create'
        ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'PLAY.PlayDashboard',
        'Submittedsongs',
        [
                'SubmittedSongs' => 'show,edit,approve'
        ],
        [
                'SubmittedSongs' => 'show,edit,approve'
        ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'PLAY.PlayDashboard',
        'Editprofile',
        [
                'EditProfile' => 'show,create'
        ],
        [
                'EditProfile' => 'show,create'
        ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'PLAY.PlayDashboard',
        'Editsong',
        [
                'EditSong' => 'show'
        ],
        [
                'EditSong' => 'show'
        ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'PLAY.PlayDashboard',
        'Register',
        [
                'Register' => 'create, confirm, confirmed'
        ],
        [
                'Register' => 'create, confirm, confirmed'
        ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'PLAY.PlayDashboard',
        'Registerconfirm',
        [
                'RegisterConfirm' => 'show'
        ],
        [
                'RegisterConfirm' => 'show'
        ]
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'PLAY.PlayDashboard',
        'Verify',
        [
                'Verify' => 'show,create,complete'
        ],
        [
                'Verify' => 'show,create,complete'
        ]
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'PLAY.PlayDashboard',
        'Contact',
        [
                'Contact' => 'show,create,showAll'
        ],
        [
                'Contact' => 'show,create,showAll'
        ]
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'PLAY.PlayDashboard',
        'Credits',
        [
                'Credits' => 'show,create,free,starter,professional,proArtist'
        ],
        [
                'Credits' => 'show,create,free,starter,professional,proArtist'
        ]
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'PLAY.PlayDashboard',
        'ContactSupport',
        [
                'ContactSupport' => 'show,create'
        ],
        [
                'ContactSupport' => 'show,create'
        ]
);

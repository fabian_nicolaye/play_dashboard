<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'PLAY.PlayDashboard',
    'Dashboard',
    'Play Dashboard'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'PLAY.PlayDashboard',
    'Listsong',
    'List Song'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'PLAY.PlayDashboard',
        'SubmitSong',
        'Submit Song'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'PLAY.PlayDashboard',
        'SubmittedSongs',
        'Submitted Songs'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'PLAY.PlayDashboard',
        'EditProfile',
        'Edit Profile'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'PLAY.PlayDashboard',
        'EditSong',
        'Edit Song'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'PLAY.PlayDashboard',
        'Register',
        'Register'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'PLAY.PlayDashboard',
        'RegisterConfirm',
        'Register Confirm'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'PLAY.PlayDashboard',
        'Verify',
        'Verify Account'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'PLAY.PlayDashboard',
        'Contact',
        'Contact'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'PLAY.PlayDashboard',
        'Credits',
        'Credits'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'PLAY.PlayDashboard',
        'ContactSupport',
        'ContactSupport'
);
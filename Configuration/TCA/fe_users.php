<?php

/**
 * TCA Overrides
 */

defined('TYPO3_MODE') or die();

call_user_func(
        function () {
            $table = 'fe_users';
            $columnArray = [
                    'credits' => [
                            'exclude' => 1,
                            'label' => 'Credits',
                            'config' => [
                                    'type' => 'input',
                                    'size' => 30,
                                    'eval' => 'trim'
                            ],
                    ],
                    'verified' => [
                            'exclude' => 1,
                            'label' => 'Verified',
                            'config' => [
                                    'type' => 'check',
                            ]
                    ],
                    'artist_name' => [
                            'exclude' => 1,
                            'label' => 'Artist Name',
                            'config' => [
                                    'type' => 'input',
                                    'size' => 30,
                                    'eval' => 'trim'
                            ],
                    ],
                    'plan' => [
                            'exclude' => 1,
                            'label' => 'Plan',
                            'config' => [
                                    'type' => 'input',
                                    'size' => 30,
                                    'eval' => 'trim'
                            ],
                    ],
            ];
            if (count($columnArray)) {
                \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
                        $table,
                        $columnArray
                );

                \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
                        $table,
                        $newFieldsString = implode(',', array_keys($columnArray)),
                        $typeList = '',
                        $position = 'after:company'
                );
            }
        }
);

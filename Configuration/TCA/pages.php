<?php

/**
 * TCA Overrides
 */

defined('TYPO3_MODE') or die();

call_user_func(
        function () {
            $table = 'pages';
            $columnArray = [
                    'icon_name' => [
                            'exclude' => 1,
                            'label' => 'Icon',
                            'config' => [
                                    'type' => 'input',
                                    'size' => 30,
                                    'eval' => 'trim'
                            ],
                    ],
            ];
            if (count($columnArray)) {
                \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
                        $table,
                        $columnArray
                );

                \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
                        $table,
                        $newFieldsString = implode(',', array_keys($columnArray)),
                        $typeList = '',
                        $position = 'after:title'
                );
            }
        }
);

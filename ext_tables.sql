#
# Table structure for table 'tx_playdashboard_domain_model_song'
#
CREATE TABLE tx_playdashboard_domain_model_song (

uid int(11) NOT NULL auto_increment,
pid int(11) DEFAULT '0' NOT NULL,

fe_user int(11) unsigned DEFAULT '0',
status int(1) unsigned DEFAULT '0',
song_name varchar(256),
song_cover varchar(256),
song_file varchar(256),
feature varchar(256),

tstamp int(11) unsigned DEFAULT '0' NOT NULL,
crdate int(11) unsigned DEFAULT '0' NOT NULL,
deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
starttime int(11) unsigned DEFAULT '0' NOT NULL,
endtime int(11) unsigned DEFAULT '0' NOT NULL,

t3ver_oid int(11) DEFAULT '0' NOT NULL,
t3ver_id int(11) DEFAULT '0' NOT NULL,
t3ver_wsid int(11) DEFAULT '0' NOT NULL,
t3ver_label varchar(255) DEFAULT '' NOT NULL,
t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
t3ver_stage int(11) DEFAULT '0' NOT NULL,
t3ver_count int(11) DEFAULT '0' NOT NULL,
t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
t3ver_move_id int(11) DEFAULT '0' NOT NULL,

t3_origuid int(11) DEFAULT '0' NOT NULL,
sys_language_uid int(11) DEFAULT '0' NOT NULL,
l10n_parent int(11) DEFAULT '0' NOT NULL,
l10n_diffsource mediumblob,

PRIMARY KEY (uid),
KEY parent (pid),
KEY t3ver_oid (t3ver_oid,t3ver_wsid),
KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'fe_users'
#
CREATE TABLE fe_users (
    credits int(11) unsigned DEFAULT '2' NOT NULL,
    verified int(1) unsigned DEFAULT '0' NOT NULL,
    artist_name varchar(256),
    plan varchar(256) DEFAULT 'free' NOT NULL,
    register_token varchar(256),
    instagram varchar(256) DEFAULT '0' NOT NULL

);

#
# Table structure for table 'pages'
#
CREATE TABLE pages (
icon_name varchar(256),

);

#
# Table structure for table 'tx_playdashboard_domain_model_messages'
#
CREATE TABLE tx_playdashboard_domain_model_messages (

uid int(11) NOT NULL auto_increment,
pid int(11) DEFAULT '0' NOT NULL,

from_user_id varchar(256) DEFAULT NULL,
to_user_id varchar(256) DEFAULT NULL,
message varchar(256) DEFAULT NULL,

tstamp int(11) unsigned DEFAULT '0' NOT NULL,
crdate int(11) unsigned DEFAULT '0' NOT NULL,
deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
starttime int(11) unsigned DEFAULT '0' NOT NULL,
endtime int(11) unsigned DEFAULT '0' NOT NULL,

t3ver_oid int(11) DEFAULT '0' NOT NULL,
t3ver_id int(11) DEFAULT '0' NOT NULL,
t3ver_wsid int(11) DEFAULT '0' NOT NULL,
t3ver_label varchar(255) DEFAULT '' NOT NULL,
t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
t3ver_stage int(11) DEFAULT '0' NOT NULL,
t3ver_count int(11) DEFAULT '0' NOT NULL,
t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
t3ver_move_id int(11) DEFAULT '0' NOT NULL,

t3_origuid int(11) DEFAULT '0' NOT NULL,
sys_language_uid int(11) DEFAULT '0' NOT NULL,
l10n_parent int(11) DEFAULT '0' NOT NULL,
l10n_diffsource mediumblob,

PRIMARY KEY (uid),
KEY parent (pid),
KEY t3ver_oid (t3ver_oid,t3ver_wsid),
KEY language (l10n_parent,sys_language_uid)

);